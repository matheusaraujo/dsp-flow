# inicializando
```shell
git init
git checkout -b prd
git add .
git commit -m "initial commit"
git remote add origin https://gitlab.com/matheusaraujo/dsp-flow.git
git push -u origin prd
```

# criando branchs básicos
```shell
git checkout -b stg
git push -u origin stg
git checkout -b dev
git push -u origin dev
```

# criando branch feat
```shell
git checkout -b feat/feat1
git add .
git commit -m "feat1.1"
git push -u feat/feat1
```